package com.example.demo.Runner;

import com.example.demo.Repository.GameRepo;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements ApplicationRunner {

    private final GameRepo gameRepo;

    public AppRunner(GameRepo gameRepo) {
        this.gameRepo = gameRepo;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("maven app");
        System.out.println(gameRepo.findById(1));
    }
}
