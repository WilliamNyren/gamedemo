package com.example.demo.Service;

import com.example.demo.Models.Game;
import com.example.demo.Repository.GameRepo;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class GameService {
    private final GameRepo gameRepo;

    public GameService(GameRepo gameRepo) {
        this.gameRepo = gameRepo;
    }

    public Game findById(int id) {
        return gameRepo.findById(id).get();
    }

    public Set<Game> findByName(String name) {
        return gameRepo.findByName(name);
    }
}
