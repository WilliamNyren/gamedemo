package com.example.demo.Repository;

import com.example.demo.Models.Game;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface GameRepo extends JpaRepository<Game, Integer> {
    Set<Game> findByName(String name);
}
