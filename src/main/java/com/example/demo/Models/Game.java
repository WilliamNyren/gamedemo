package com.example.demo.Models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.UUID;

@Entity
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    private UUID uuid;
    private int rating;
    private String genre;

    public UUID getId() {
        return uuid;
    }
    public Game() {}
    public Game(String test) {

        this.uuid = UUID.randomUUID();
        this.name = test;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Game object:");
        builder.append("\n- ID: " + id);
        builder.append("\n- UUID: " + uuid);
        builder.append("\n- name: " + name);

        return builder.toString();
    }

}
